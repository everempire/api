import { Builder } from '@gamedevfox/katana';
import { hashSet } from '@kitsune-system/common';

import { NodeId } from '../types';

export const writeSet = async (build: Builder) => {
  const writeEdge = await build('writeEdge');

  return (set: NodeId[]) => {
    const id = hashSet(set);
    const edgePs = set.map(node => writeEdge([id, node]));

    return Promise.all(edgePs).then(() => id);
  };
};

export const readSet = async (build: Builder) => build('listTails');
