import express from 'express';

import { Builder } from '@gamedevfox/katana';

const path = '/set';

export const builder = async (build: Builder) => {
  const [writeSet, readSet] = await build(['writeSet', 'readSet']);

  const router = express.Router();

  router.put(path, (req, res, next) => {
    const set = req.body;
    writeSet(set).then((id: any) => res.json(id)).catch(next);
  });

  router.get(path, (req, res, next) => {
    const id = req.body;
    readSet(id).then((result: any) => res.json(result)).catch(next);
  });

  router.get('/node/:id/set', (req, res, next) => {
    const { id } = req.params;
    readSet(id).then((result: any) => res.json(result)).catch(next);
  });

  return router;
};
