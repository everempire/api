import { Pipe } from '@kitsune-system/common';

export const Variable = () => {
  let val: any = null;

  const [output, onOutput] = Pipe();

  const input = (value: any) => {
    val = value;
    output(val);
  };

  const get = (fn: (value: any) => void) => fn(val);

  return { input, get, onOutput };
};
