import { Bucket, ListCollector } from '@kitsune-system/common';
import { expect } from 'chai';

import { Variable } from './variable';

describe('Systems', () => {
  describe('Variable', () => {
    it('should work', done => {
      const v = Variable();

      const c: any = ListCollector();
      const b = Bucket();

      v.onOutput(b)

      v.get(c());
      v.input(123);
      v.get(c());
      v.input(456);
      v.get(c());
      v.input(789);
      v.get(c());

      expect(b.empty()).to.deep.equal([123, 456, 789]);

      c.done((values: any) => {
        expect(values).to.deep.equal([null, 123, 456, 789]);
        done();
      });
    });
  });
});
