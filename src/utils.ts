import globby from 'globby';

import { AsyncBuilder } from '@gamedevfox/katana';
import { initDB, isInitialized } from './data/db';
import { Data } from './types';

interface Config {
  knexConfig: any;

  data?: Data;
  dataDir?: string;
  dataFile?: string;

  builderConfig?: any;
}

export const initBuilder = async (config: Config) => {
  const { knexConfig, data, dataDir, dataFile, builderConfig } = config;

  const buildConfig = await loadModules(`${__dirname}/**/*.build.js`);
  const build = AsyncBuilder({
    knexConfig,
    ...buildConfig,
    ...builderConfig,
  });

  const db = await build('db');

  const initialized = await isInitialized(db);
  if(initialized)
    return build;

  await initDB(db, knexConfig);

  if(data) {
    const populateDB = await build('populateDB');
    await populateDB(data);
  }

  if(dataDir) {
    const populateDBFromDir = await build('populateDBFromDir');
    await populateDBFromDir(dataDir);
  }

  if(dataFile) {
    const populateDBFromFile = await build('populateDBFromFile');
    await populateDBFromFile(dataFile)
  }

  return build;
};

export const loadModules = async (glob: string) => {
  const paths = await globby(glob);

  const buildConfig: any = {};
  paths.forEach(path => {
    const module = require(path);
    Object.entries(module).forEach(([key, value]) => {
      if(key in buildConfig)
        throw new Error(`Duplicate key "${key}" in module "${path}"`);

      buildConfig[key] = value;
    });
  });
  return buildConfig;
};
