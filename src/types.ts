export interface StringMap { [key: string]: string }

export type Data = { edges: Edge[], strings: String[] };
export type Edge = { id?: string, head: string, tail: string };
export type NodeId = string;
export type String = { id?: string, string: string };
