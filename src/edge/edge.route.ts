import express from 'express';

import { Builder } from '@gamedevfox/katana';

import { Edge } from '../types';

const path = '/edge';

export const builder = async (build: Builder) => {
  const [writeEdge, destroyEdge, readEdge, listEdges] = await build(['writeEdge', 'destroyEdge', 'readEdge', 'listEdges']);

  const router = express.Router();

  router.put(path, async (req, res, next) => {
    const { head, tail } = req.body;
    writeEdge([head, tail]).then((id: any) => res.json(id)).catch(next);
  });

  router.delete(path + '/:id', (req, res, next) => {
    const { id } = req.params;
    destroyEdge(id).then(() => res.send()).catch(next);
  });

  router.get(path, (req, res, next) => {
    listEdges().then((edges: Edge[]) => res.json(edges)).catch(next);
  });

  router.get('/node/:id/edge', (req, res, next) => {
    const { id } = req.params;
    readEdge(id).then((result: Edge) => res.json(result)).catch(next);
  });

  return router;
};
