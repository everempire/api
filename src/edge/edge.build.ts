import { hashEdge } from '@kitsune-system/common';

import { Builder } from '@gamedevfox/katana';

import { edgeTable } from '../config';
import { NodeId } from '../types';

export const listEdges = async (build: Builder) => {
  const db = await build('db');
  return () => db(edgeTable);
};

export const isEdge = async (build: Builder) => {
  const db = await build('db');
  return async (id: string) => {
    const { count } = await db(edgeTable).where({ id }).first().count();
    return count === '1';
  };
};

export const writeEdge = async (build: Builder) => {
  const db = await build('db');

  return (input: [string, string] | { head: string, tail: string }) => {
    let head: string, tail: string;
    if(Array.isArray(input)) {
      if(input.length > 2) throw new Error(`Edge must be of length 2: ${JSON.stringify(input)}`);
      [head, tail] = input;
    } else {
      head = input.head;
      tail = input.tail;
    }

    const id = hashEdge([head, tail]);

    // TODO: Can this be OPTIMIZED into one query?
    return db(edgeTable).where({ id }).then((result: any) => {
      if(result.length)
        return id;

      const newObj = { id, head, tail };
      return db(edgeTable).insert(newObj).then(() => id);
    });
  };
};

export const destroyEdge = async (build: Builder) => {
  const db = await build('db');
  return (id: NodeId) => db(edgeTable).where({ id }).delete();
};

export const readEdge = async (build: Builder) => {
  const db = await build('db');
  return (id: NodeId) => db(edgeTable).where({ id }).first();
};

export const listHeads = async (build: Builder) => {
  const db = await build('db');
  return (tail: NodeId) => db(edgeTable).where({ tail }).pluck('head');
};

export const listTails = async (build: Builder) => {
  const Select = await build('Select');
  return (head: NodeId) => Select(head).tails().select();
};
