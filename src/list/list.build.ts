import { Builder } from '@gamedevfox/katana';
import { hashEdge, hashList } from '@kitsune-system/common';

import { NodeId } from '../types';

export const writeList = async (build: Builder) => {
  const writeEdge = await build('writeEdge');

  return async (list: NodeId[]) => {
    const listId = hashList(list);

    let head = listId;
    for (const item of list) {
      head = await writeEdge({ head, tail: item });
    }

    return listId;
  };
};

export const readList = async (build: Builder) => {
  const listTails = await build('listTails');

  return async (listId: NodeId) => {
    let tail = listId;

    const result: NodeId[] = [];
    while (true) {
      const tails = await listTails(tail);

      if (tails.length === 0)
        break;

      const [newTail] = tails;
      result.push(newTail);
      tail = hashEdge([tail, newTail]);
    }

    return result;
  };
};
