import { expect } from 'chai';

import { edgeTable, sqlite3Config } from '../config';
import { initBuilder } from '../utils';

describe('List', () => {
  it('should work', async () => {
    const build = await initBuilder({ knexConfig: sqlite3Config });
    const [writeList, readList, db] = await build(['writeList', 'readList', 'db']);

    const list = ['ALPHA', 'BETA', 'DELTA', 'OMEGA'];

    const listId = await writeList(list);
    expect(listId).to.equal('KALHfF1pY30GAz/GGXktWoecdtmaTiEnyrwexBLeTaQ=');

    const edges = await db(edgeTable);
    expect(edges).to.deep.equal([
      {
        id: 'eGJtqGHETE5m5DNvm4dcr41Lb/+IAcuaKwZyVSD6mdE=',
        head: 'KALHfF1pY30GAz/GGXktWoecdtmaTiEnyrwexBLeTaQ=',
        tail: 'ALPHA'
      },
      {
        id: 'p+5JCzLYVEQK8Dd1rtsAzc0BjkZXzAaEY/9+L8Yqmw0=',
        head: 'eGJtqGHETE5m5DNvm4dcr41Lb/+IAcuaKwZyVSD6mdE=',
        tail: 'BETA'
      },
      {
        id: 'UIi2FwfPRPCOYRRTDhFHL0HxSMdiSLIBBevJzEXeHvo=',
        head: 'p+5JCzLYVEQK8Dd1rtsAzc0BjkZXzAaEY/9+L8Yqmw0=',
        tail: 'DELTA'
      },
      {
        id: '01VqXKrBRB6OVq1R14tJ0hlesNYzdbhSlnSdfrN+PWg=',
        head: 'UIi2FwfPRPCOYRRTDhFHL0HxSMdiSLIBBevJzEXeHvo=',
        tail: 'OMEGA'
      }
    ]);

    const newList = await readList(listId);
    expect(newList).to.deep.equal(list);

    db.destroy();
  });
});
