import { Builder } from '@gamedevfox/katana';
import express from 'express';

const path = '/map';

export const builder = async (build: Builder) => {
  const writeMap = await build('writeMap');

  const router = express.Router();

  router.put(path, (req, res, next) => {
    const map = req.body;
    writeMap(map).then((id: string) => res.json(id)).catch(next);
  });

  return router;
};
