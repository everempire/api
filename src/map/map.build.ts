import { Builder } from '@gamedevfox/katana';
import { NodeId } from '../types';

export const writeMap = async (build: Builder) => {
  const [writeEdge, writeSet] = await build(['writeEdge', 'writeSet']);

  return (map: any) => {
    const subEdgePs = Object.entries(map)
      .map(([key, value]) => writeEdge([key, value]));

    return Promise.all(subEdgePs)
      .then(writeSet)
      .then(setId => {
        return setId;
      });
  };
};

export const readMap = async (build: Builder) => {
  const [readEdge, readSet] = await build(['readEdge', 'readSet']);

  return async (mapId: NodeId) => {
    const set = await readSet(mapId);
    const edgePs = set.map(readEdge);

    const result: any = {};
    (await Promise.all(edgePs)).forEach((edge: any) => {
      result[edge.head] = edge.tail;
    });
    return result;
  };
};
