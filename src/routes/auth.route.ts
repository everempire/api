import express from 'express';
import Knex from 'knex';

import { JWT, nowSeconds } from '../jwt';

const { generate } = JWT;

const EXPIRE_TIME = 5 * 60; // 5 minutes

export const build = (db: Knex) => {
  const auth = express.Router();

  auth.post('/auth', async (req, res) => {
    const { email } = req.body;

    const result = await db('user').where({ email });
    if(result.length) {
      res.status(401).send(`No such account for email: ${email}`);
      return;
    }

    const user = result[0];

    const exp = nowSeconds() + EXPIRE_TIME;
    const token = generate({ sub: user.id, email: user.email, name: user.name, exp });

    res.json(token);
  });

  return auth;
};
