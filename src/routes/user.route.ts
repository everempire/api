import express from 'express';

import { Builder } from '@gamedevfox/katana';

import { userTable } from '../config';
import { randomId } from '../random';

const path = '/user';

export const build = async (build: Builder) => {
  const db = await build('db');

  const user = express.Router();

  user.post(path, (req, res) => {
    const id = randomId();
    const { email, name } = req.body;

    const user = { id, email, name };
    db(userTable).insert(user)
      .then(() => res.json(id))
      .catch(() => res.status(400).send());
  });

  user.get(path, (req, res) => {
    db(userTable).then((result: any) => res.json(result));
  });

  return user;
};
