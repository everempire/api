import { Router } from 'express';

import { JWT, nowSeconds } from '../jwt';
import { randomId } from '../random';

const { parse, verify } = JWT;

export const route = () => {
  const route = Router();

  route.use('/random', (req, res) => {
    const id = randomId();
    res.json(id);
  });

  route.use('/me', (req, res) => {
    const { authorization } = req.headers;
    if(!authorization) {
      res.sendStatus(401);
      return;
    }

    const token = Array.isArray(authorization) ? authorization[0] : authorization;

    const payload: any = parse(token);
    payload.isValid = verify({ token, now: nowSeconds() });

    res.json(payload);
  });

  return route;
};

