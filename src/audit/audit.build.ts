import { Builder } from '@gamedevfox/katana';
import { MASTER, SET } from '@kitsune-system/common';

import { NAME } from '@kitsune-system/common';
import { auditTable, stringTable } from '../config';
import { edges } from '../data/db';
import { NATIVE_FN, READ_FN } from '../nodes';

export interface Query {
  name?: string;
  selection?: any;
  dependants?: string[];
}

export const setQueries = async (build: Builder) => {
  const Select = await build('Select');

  return (setId: string, name: string) => [{
    name: `${name}-set-edge`,
    selection: Select(setId).tails().asEdge().select(),
  }, {
    name: `${name}-set-node`,
    selection: Select(setId).tails().select(),
  }];
}

export const nameQueries = async (build: Builder) => {
  const Select = await build('Select');

  return (auditLabel?: string) => {
    const prefix = auditLabel ? `${auditLabel}-` : '';

    return [{
      name: `${prefix}name-edge`,
      selection: Select.and(
        Select(NAME).tails().asEdge(),
        Select.audit(auditLabel).heads().asEdge(),
      ).select(),
    }, {
      name: `${prefix}name-sub-edge`,
      selection: Select.audit(`${prefix}name-edge`).tails().asEdge().select(),
      dependants: [`${prefix}name-edge`],
    }, {
      name: `${prefix}name-string`,
      selection: Select.audit(`${prefix}name-edge`).tails().select(),
      dependants: [`${prefix}name-edge`],
    }];
  };
};

export const listAuditEdges = async (build: Builder) => {
  const db = await build('db');

  return (sorted = false) => {
    let result = edges(db).whereIn('id', db => db.from(auditTable).select('id'));

    if(sorted)
      result = result.orderBy('head');

    return result;
  };
};

export const listAuditStrings = async (build: Builder) => {
  const db = await build('db');

  return (sorted = false) => {
    let result = db(stringTable).whereIn('id', (db: any) => db.from(auditTable).select('id'));

    if(sorted)
      result = result.orderBy('string');

    return result;
  };
};

export const listAuditLabels = async (build: Builder) => {
  const db = await build('db');
  return () => db.from(auditTable).distinct('label').pluck('label');
};

export const listAuditLabelNodes = async (build: Builder) => {
  const db = await build('db');
  return (label: string) => db.from(auditTable).where({ label }).pluck('id');
};

export const listRemainderNodes = async (build: Builder) => {
  const db = await build('db');

  return () => edges(db).whereNotIn('id',
    db => db.from(auditTable).select('id')
  ).pluck('id');
};

export const auditClear = async (build: Builder) => {
  const db = await build('db');
  return () => db.from(auditTable).delete();
};

// //////////////////////////////////////////

export const runAuditQueries = async (build: Builder) => {
  const [Select, db, auditClear, setQueries, nameQueries] =
    await build(['Select', 'db', 'auditClear', 'setQueries', 'nameQueries']);

  const auditQueries = [
    ...setQueries(SET, 'set'),
    ...setQueries(MASTER, 'master'),
    ...setQueries(READ_FN, 'read-fn'),
    ...setQueries(NATIVE_FN, 'native-fn'),

    {
      name: 'read-fn-sub-edge',
      selection: Select.audit('read-fn-set-edge').tails().asEdge().select(),
      dependants: ['read-fn-set-edge'],
    },

    ...nameQueries(),
  ];

  const auditQueryMap: any = {};
  auditQueries.forEach(query => auditQueryMap[query.name] = query);

  return async () => {
    const results: any = {};

    await auditClear();

    const runQuery = async ({ name, selection, dependants = [] }: Query): Promise<any> => {
      const runNextDep = async (): Promise<any> => {
        if(dependants.length === 0)
          return Promise.resolve();

        const depName = dependants.shift();
        if(depName === undefined)
          throw new Error(`dependant is undefined: ${depName}`);

        const dep = auditQueryMap[depName];

        await runQuery(dep);

        return await runNextDep();
      };

      await runNextDep();

      if(name === undefined || name in results)
        return;

      let query = db.from(auditTable);
      query = query.insert(selection.select(db.raw(':name as "label"', { name })));

      const result = await query;
      results[name] = result;
      return result;
    };

    const names = auditQueries.map(query => query.name);
    await runQuery({ dependants: names });

    return results;
  };
};
