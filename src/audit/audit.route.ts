import express from 'express';

import { Builder } from '@gamedevfox/katana';

const path = '/audit';

export const builder = async (build: Builder) => {
  const [
    runAuditQueries, listAuditEdges, listAuditStrings,
    listAuditLabels, listAuditLabelNodes, listRemainderNodes,
  ] = await build([
    'runAuditQueries', 'listAuditEdges', 'listAuditStrings',
    'listAuditLabels', 'listAuditLabelNodes', 'listRemainderNodes',
  ]);

  const router = express.Router();

  router.get(path, (req, res, next) => {
    runAuditQueries()
      .then((results: any) => {
        const response: any = {};
        Object.entries(results).forEach(([name, result]: [string, any]) => {
          response[name] = result.rowCount;
        });
        res.json(response);
      })
      .catch(next);
  });

  router.get(path + '/edge', (req, res, next) => {
    listAuditEdges()
      .then((result: any) => res.json(result))
      .catch(next);
  });

  router.get(path + '/string', (req, res, next) => {
    listAuditStrings()
      .then((result: any) => res.json(result))
      .catch(next);
  });

  router.get(path + '/label', (req, res, next) => {
    listAuditLabels()
      .then((result: any) => res.json(result))
      .catch(next);
  });

  router.get(path + '/label/:label', (req, res, next) => {
    const { label } = req.params;

    listAuditLabelNodes(label)
      .then((result: any) => res.json(result))
      .catch(next);
  });

  router.get(path + '/remainder', (req, res, next) => {
    listRemainderNodes()
      .then((result: any) => res.json(result))
      .catch(next);
  });

  return router;
};
