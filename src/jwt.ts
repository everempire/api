import { KJUR } from 'jsrsasign';

const { sign, parse: parseToken, verifyJWT } = KJUR.jws.JWS;

const HEADER = { alg: 'HS256', typ: 'JWT' };

export const nowSeconds = () => Math.floor(Date.now() / 1000);

export const build = (secretKey: string) => {
  const generate = (payload: any) => sign('HS256', HEADER, payload, secretKey);

  const parse = (token: string) => parseToken(token).payloadObj;

  const verify = ({ token, now }: { token: string, now: number }) => {
    return verifyJWT(token, secretKey, { alg: ['HS256'], verifyAt: now });
  };

  return { generate, parse, verify };
};

export const JWT = build('dev-secret');
