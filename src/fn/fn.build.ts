import { Builder } from '@gamedevfox/katana';

import { NodeId, StringMap } from '../types';

export const readFnCall = async (build: Builder) => {
  const [readEdge, graphBuild] = await build(['readEdge', 'graphBuild']);

  return async (fnCallId: NodeId) => {
    const { head: fnId, tail: argsId } = await readEdge(fnCallId);

    const fn = await graphBuild(fnId);
    const args = await graphBuild(argsId);

    return () => fn(args);
  };
};

export const execute = async (build: Builder) => {
  const [graphBuild, readEdge, readMap] = await build(['graphBuild', 'readEdge', 'readMap']);

  const evaluate = async (id: string, context: any) => {
    let result = context[id];

    if(!result)
      result = await graphBuild(id);

    if(!result)
      result = execute(id, context);

    return result;
  };

  const execute = async (fnCallId: NodeId, context: any = {}) => {
    if(fnCallId in context)
      return context[fnCallId];

    const fnCallEdge = await readEdge(fnCallId);
    if(!fnCallEdge)
      throw new Error(`No such edge: ${fnCallId}`);

    const { head: fnId, tail: argMapId } = fnCallEdge;

    const fn = await evaluate(fnId, context);

    const args = await readMap(argMapId);
    const argsEntries = Object.entries(args) as [string, string][];

    const input: any = {};
    for(const entries of argsEntries) {
      const [key, value] = entries;
      const result = await evaluate(value, context);
      input[key] = result;
    }

    const result = await fn(input);
    context[fnCallId] = result;
    return result;
  };

  return execute;
};

export const executeSet = async (build: Builder) => {
  const [execute, readSet] = await build(['execute', 'readSet']);

  return async (callSetId: NodeId, context: StringMap = {}) => {
    const callSet: NodeId[] = await readSet(callSetId);

    const result: StringMap = {};
    await Promise.all(callSet.map(async call => {
      const innerContext = { ...context };
      result[call] = await execute(call, innerContext);
    }));

    return result;
  };
};

export const executeList = async (build: Builder) => {
  const [execute, readList] = await build(['execute', 'readList']);

  return async (callListId: NodeId, context: StringMap = {}) => {
    const callList: NodeId[] = await readList(callListId);

    const result: StringMap = {};
    for(const call of callList) {
      const innerContext = { ...context };
      result[call] = await execute(call, innerContext);
    }

    return result;
  };
};
