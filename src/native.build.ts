import { Builder } from '@gamedevfox/katana';
import { CONSOLE_LOG, READ_FN_CALL, READ_LIST, READ_MAP, READ_STRING } from './nodes';

const aliases = {
  readList: READ_LIST,
  readMap: READ_MAP,
  readString: READ_STRING,
  readFnCall: READ_FN_CALL,
};

const aliasBuilders: any = {};
Object.entries(aliases).forEach(([name, id]) => {
  aliasBuilders[id] = (build: Builder) => build(name);
});

module.exports = {
  [CONSOLE_LOG]: () => (args: any[]) => console.log(...args),
  ...aliasBuilders,
};
