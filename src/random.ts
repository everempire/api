import crypto from 'crypto';

const BIT_SIZE = 256;
const BYTE_SIZE = Math.ceil(BIT_SIZE / 8);

export const randomId = () => crypto.randomBytes(BYTE_SIZE).toString('base64');
