
import { LIST_N } from '@kitsune-system/common';

import { sqlite3Config } from './config';
import { CONSOLE_LOG, FN_CALL } from './nodes';
import { initBuilder } from './utils';

const YOU = 'ID_FOR_YOU';
const ME = 'ID_FOR_ME';

const ADAM = 'ID_FOR_ADAM';
const EVE = 'ID_FOR_EVE';
const JACK = 'ID_FOR_JACK';
const JILL = 'ID_FOR_JILL';

const SAY_HELLO = 'ID_FOR_SAY_HELLO';
const SAY_GOODBYE = 'ID_FOR_SAY_GOODBYE';

let hello = 0;

const builderConfig: any = {
  [SAY_HELLO]: () => async (context: any) => {
    const { [YOU]: you, [ME]: me } = context;

    const msg = `${++hello}: Hello ${you}, I'm ${me}.`;
    console.log(msg);

    await new Promise(resolve => {
      const rando = Math.random() * 100
      setTimeout(resolve, rando);
    });

    return hello;
  },

  [SAY_GOODBYE]: () => (context: any) => {
    const { [YOU]: you } = context;

    const msg = `${++hello}: Goodbye ${you}, it was nice to meet you.`;
    console.log(msg);

    return msg;
  },
};

export const FN = 'XhGhUlkuFDno8KseSMlwEks9uq61BremJCBIkuUatwA=';

export const AFTER = 'ZRGZ6chNC8dmbNTBiCTJPJYwOCA0KY4HtZ+UTqAiwTw=';

describe('Dummy', () => {
  it('another one', async () => {
    const build = await initBuilder({ knexConfig: sqlite3Config, dataDir: 'data', builderConfig });
    const [Create, execute, executeSet, executeList]: any
      = await build(['Create', 'execute', 'executeSet', 'executeList']);

    const sayHelloA = await Create(SAY_HELLO, { [YOU]: ADAM, [ME]: EVE });
    const sayHelloB = await Create(SAY_HELLO, { [YOU]: EVE, [ME]: sayHelloA });
    const sayHelloC = await Create(SAY_HELLO, { [YOU]: JACK, [ME]: sayHelloA, [AFTER]: sayHelloB });
    const sayGoodbye = await Create(SAY_GOODBYE, { [YOU]: sayHelloB, [ME]: JILL, [AFTER]: sayHelloC });

    const context: any = { [ADAM]: 'Adam', [EVE]: 'Eve', [JACK]: 'Jack', [JILL]: 'Jill' };

    console.log('== EXECUTE SINGLE ==');
    const singleContext = { ...context };
    const single = await execute(sayGoodbye, singleContext);
    console.log('CONTEXT:', singleContext);
    console.log('== SINGLE:', single);
    console.log();

    console.log('== EXECUTE SET ==');
    const callSetId = await Create.set(sayHelloC, sayGoodbye, sayHelloA, sayHelloB);
    const setContext = { ...context };
    const set = await executeSet(callSetId, setContext);
    console.log('CONTEXT:', setContext);
    console.log('== SET:', set);
    console.log();

    console.log('== EXECUTE LIST ==');
    const callListId = await Create.list(sayHelloA, sayHelloB, sayHelloC, sayGoodbye);
    const listContext = { ...context };
    const list = await executeList(callListId, listContext);
    console.log('CONTEXT:', listContext);
    console.log('== LIST:', list);
    console.log();
  });

  it('should work', async () => {
    const build = await initBuilder({ knexConfig: sqlite3Config, dataDir: 'data' });
    const [Create, graphBuild] = await build(['Create', 'graphBuild']);

    // const stringId = await Create.string('Hello, Kitsune');
    // const stringEdge = await Create.edge(STRING, stringId);

    const listId = await Create.list('ALPHA', 'BETA', 'DELTA', 'OMEGA');
    const listEdge = await Create.edge(LIST_N, listId);

    // const mapId = await Create.map({ hello: 'world', goodbye: 'moon' });
    // const mapEdge = await Create.edge(MAP_N, mapId);

    const fnCall = await Create.edge(CONSOLE_LOG, listEdge);
    const fnCallBuild = await Create.edge(FN_CALL, fnCall);

    const result = await graphBuild(fnCallBuild);
    result()
  });
});
