import { Builder } from '@gamedevfox/katana';
import { hashString } from '@kitsune-system/common';

import { stringTable } from '../config';

export const listStrings = async (build: Builder) => {
  const db = await build('db');

  return (options: { fuzzy?: string, limit?: number } | void) => {
    const { fuzzy, limit } = options || {};

    let result = db(stringTable);

    if(fuzzy) {
      result = result.orderByRaw(
        db.raw('similarity(LOWER(string), LOWER(?)) desc', fuzzy)
      );
    }

    if(limit)
      result = result.limit(limit);

    return result;
  };
};

export const isString = async (build: Builder) => {
  const db = await build('db');

  return (id: string) => db(stringTable).where({ id }).first().count()
    .then(({ count }: any) => count === '1');
};

export const writeString = async (build: Builder) => {
  const db = await build('db');

  return (string: string) => {
    const id = hashString(string);

    // TODO: See if we can improve this query
    return db(stringTable).where({ id }).then((result: any) => {
      if(result.length)
        return id;

      const newObj = { id, string };
      return db(stringTable).insert(newObj).then(() => id);
    });
  };
};

export const readString = async (build: Builder) => {
  const db = await build('db');

  return (id: string) => db(stringTable).where({ id }).first().pluck('string')
    .then((row: any) => row.length === 0 ? null : row[0]);
};
