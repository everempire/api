import { Router } from 'express';

import { Builder } from '@gamedevfox/katana';

const path = '/string';

export const builder = async (build: Builder) => {
  const [listStrings, writeString, readString]
    = await build(['listStrings', 'writeString', 'readString']);

  const router = Router();

  router.get(path, (req, res, next) => {
    const { fuzzy, limit } = req.query;

    listStrings({ fuzzy, limit })
      .then((result: any) => res.json(result))
      .catch(next);
  });

  router.put(path, (req, res, next) => {
    const string = req.body;
    writeString(string).then((id: string) => res.json(id)).catch(next);
  });

  router.get('/node/:id/string', (req, res, next) => {
    const { id } = req.params;
    readString(id).then((string: string) => res.json(string)).catch(next);
  });

  return router;
};
