import Knex from 'knex';

import { Builder } from '@gamedevfox/katana';

import { Data, Edge, String } from '../types';
import { readFromDir, readFromFile } from './data';
import { bindSelect } from './old-select';
import { build as buildSelect } from './select';

export const db = async (build: Builder) => {
  const knexConfig = await build('knexConfig');
  return Knex(knexConfig);
};

export const populateDB = async (build: Builder) => {
  const [writeEdge, writeString] = await build(['writeEdge', 'writeString']);

  return async ({ edges, strings }: Data) => {
    await Promise.all((edges as Edge[]).map(({ head, tail }) => writeEdge([head, tail])));
    await Promise.all((strings as String[]).map(writeString));
  };
};

export const populateDBFromDir = async (build: Builder) => {
  const populateDB = await build('populateDB');

  return async (dataPath: string) => {
    const data = await readFromDir(dataPath);
    await populateDB(data);
  };
};

export const populateDBFromFile = async (build: Builder) => {
  const populateDB = await build('populateDB');

  return async (dataPath: string) => {
    const data = await readFromFile(dataPath);
    await populateDB(data);
  };
};

export const loadData = async (build: Builder) => {
  const [listAuditEdges, listAuditStrings] = await build(['listAuditEdges', 'listAuditStrings']);

  return async () => {
    const edgesP = listAuditEdges(true);
    const stringsP = listAuditStrings(true);

    const [edges, strings] = await Promise.all([edgesP, stringsP]);

    return { edges, strings };
  };
};

export const OldSelect = async (build: Builder) => {
  const db = await build('db');
  return bindSelect(db);
};

export const Select = async (build: Builder) => {
  const db = await build('db');
  return buildSelect(db);
};
