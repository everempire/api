import { expect } from 'chai';
import { MASTER } from '@kitsune-system/common';

import { sqlite3Config } from '../config';
import { initBuilder } from '../utils';
import { addQuery, Select } from './select';

describe('Select', () => {
  it('should work', async () => {
    const build = await initBuilder({ knexConfig: sqlite3Config, dataDir: 'data' });
    const [Select, runAuditQueries]: [Select, any] = await build(['Select', 'runAuditQueries']);

    const nodeNames = await Select(MASTER).tails('node').heads().asEdge().tails().strings({ string: 'name' }).select();
    expect(nodeNames).to.deep.equal([
      { node: '4fXleUmgAKEInL1j7LBttHmXPys4rNJLtrx6jFBW8cg=', name: 'Map' },
      { node: '9NeOSRxG9JCQMxyjn8ne/oTHxiaQRfWp4mf96DRzpa0=', name: 'Edge' },
      { node: 'L3/gS76VcmUV8NF9/MArpc6joqfzVD8jCkPHzYJ7Z8g=', name: 'Node' },
      { node: 'EKoFhEPvHQv+iecaEM6pooOStYlSrGiHozWkJgBRNiA=', name: 'System' },
      { node: 'ApI5ebUqrziXaFRfv7tYvxANvW5LGb5yRPRBanMV21c=', name: 'Master' },
      { node: '8FEjj2zgtXQxtn5RA8+hniN2MPlD2D4RkmmY1Q6+3Yc=', name: 'Function Call' },
      { node: 'MqzDonyZJMvqheNnRGHg9o7qNX+QZtK+0yfNhgOIW6s=', name: 'Native Function' },
      { node: 'PNF8apG9c4AMPN9lxSoM8MyAJ3ziWVBu7kizYxQDqvc=', name: 'Name' },
      { node: 'T5ncjTd6X9U0yxKzgDqpcW4otVk6vVGZL/vfU2TlWI4=', name: 'String' },
      { node: 'haxZgMavaTh5lUmqXk3dD+A7BViAFQKWXOv7KanC83k=', name: 'Set' },
    ]);

    const nameNodes = await Select.string('Master', 'name').heads().edges({ tail: 'node' }).select();
    expect(nameNodes).to.deep.equal([{
      name: 'XyFxsGiw/a0o3a0gu35oZJSYmnJK6QoA+y5/q67Vww4=',
      node: 'ApI5ebUqrziXaFRfv7tYvxANvW5LGb5yRPRBanMV21c=',
    }]);

    await runAuditQueries();

    const audit = await Select.audit('name-edge').edges().asTail().select();
    expect(audit).to.deep.equal([
      '4QSz4n2p6jkmOdV73q+qJi6KFyErNflcEAZe3+J4dIs=', '8FEjj2zgtXQxtn5RA8+hniN2MPlD2D4RkmmY1Q6+3Yc=',
      'PNF8apG9c4AMPN9lxSoM8MyAJ3ziWVBu7kizYxQDqvc=', 'L3/gS76VcmUV8NF9/MArpc6joqfzVD8jCkPHzYJ7Z8g=',
      'ApI5ebUqrziXaFRfv7tYvxANvW5LGb5yRPRBanMV21c=', '39MFm+zqg6GVlRX7ZPsZkNOkBGOPD9kZmKMuJoF6UM8=',
      'T5ncjTd6X9U0yxKzgDqpcW4otVk6vVGZL/vfU2TlWI4=', '9NeOSRxG9JCQMxyjn8ne/oTHxiaQRfWp4mf96DRzpa0=',
      'nKUkyYigrfLdsPi3WNFWgikkMg2q0r2+DWrxgjGvcP4=', '4fXleUmgAKEInL1j7LBttHmXPys4rNJLtrx6jFBW8cg=',
      'rPNhH/VmxaL498q9OBGtBfNy1Orz9Toh3ucojMgmLhw=', 'j3rUDCqx6P0fuTafTVZ6LkL33uGyyVi7qc+GKJzWWSA=',
      'RydpwomWq6ybZ0MaRD/2ikY1vHgnYDD2StckUtpx4GU=', 'MqzDonyZJMvqheNnRGHg9o7qNX+QZtK+0yfNhgOIW6s=',
      'EKoFhEPvHQv+iecaEM6pooOStYlSrGiHozWkJgBRNiA=', 'J6D5k8IcsRYwyIESuUB0oaGy6Y3GBA1M570Vmeykc1Q=',
      'haxZgMavaTh5lUmqXk3dD+A7BViAFQKWXOv7KanC83k=',
    ]);
  });

  const data = {
    edges: [
      { head: 'ONE', tail: 'ALPHA' },
      { head: 'ONE', tail: 'BETA' },
      { head: 'ONE', tail: 'DELTA' },

      { head: 'TWO', tail: 'BETA' },
      { head: 'TWO', tail: 'DELTA' },
      { head: 'TWO', tail: 'OMEGA' },
      { head: 'TWO', tail: 'SIGMA' },

      { head: 'THREE', tail: 'ALPHA' },
      { head: 'THREE', tail: 'OMEGA' },
      { head: 'THREE', tail: 'THETA' },
    ],
    strings: [],
  };

  it('Select.and', async () => {
    const build = await initBuilder({ knexConfig: sqlite3Config, data });
    const Select: Select = await build('Select');

    const nodes = await Select.and(
      Select('ONE').tails({ head: 'one', tail: 'two' }),
      Select('TWO').tails({ id: 'three', head: 'four' })
    ).heads().select();

    expect(nodes).to.deep.equal(['ONE', 'TWO']);

    const nodes2 = await Select.and(
      Select('OMEGA').heads({ head: 'one', tail: 'two' }),
      Select('ALPHA').heads({ id: 'three', head: 'four' }),
    ).select();

    expect(nodes2).to.deep.equal(['THREE']);
  });

  it('Select.or', async () => {
    const build = await initBuilder({ knexConfig: sqlite3Config, data });
    const Select: Select = await build('Select');

    const nodes = await Select.or(
      Select('ONE').tails({ head: 'one', tail: 'two' }),
      Select('TWO').tails({ id: 'three', head: 'four' }),
      Select('THREE').tails(),
    ).select();

    expect(nodes).to.deep.equal(['ALPHA', 'BETA', 'DELTA', 'OMEGA', 'SIGMA', 'THETA']);

    const nodes2 = await Select.or(
      Select('ALPHA').heads({ head: 'one', tail: 'two' }),
      Select('THETA').heads(),
    ).tails().select();

    expect(nodes2).to.deep.equal(['ALPHA', 'BETA', 'DELTA', 'OMEGA', 'THETA']);
  });

  it('addQuery', () => {
    let queries: any[] = [];

    queries = addQuery(queries, {
      select: [],
    } as any);

    queries = addQuery(queries, {
      select: ['targetName'],
      target: 'target',
    } as any);

    queries = addQuery(queries, {
      select: ['head', 'tail'],
    } as any);

    queries = addQuery(queries, {
      select: ['id', { head: 'mine', tail: 'yours' }],
    } as any);

    expect(queries).to.deep.equal([
      { select: {}, tableAlias: 'table1' },
      { select: { targetName: 'table2.target' }, tableAlias: 'table2', target: 'target' },
      {
        select: {
          'head': 'table3.head',
          'tail': 'table3.tail',
        },
        tableAlias: 'table3',
      },
      {
        select: {
          mine: 'table4.head',
          id: 'table4.id',
          yours: 'table4.tail',
        },
        tableAlias: 'table4',
      },
    ]);
  });
});
