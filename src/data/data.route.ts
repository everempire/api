import { promises } from 'fs';
import { Router } from 'express';

import { Builder } from '@gamedevfox/katana';

import { dataPath } from '../config';
import { writeToDir } from './data';

const { mkdir } = promises;

export const builder = async (build: Builder) => {
  const [runAuditQueries, loadData] = await build(['runAuditQueries', 'loadData']);

  const router = Router();

  router.use('/data/save', (req, res, next) => {
    mkdir(dataPath, { recursive: true })
      .then(runAuditQueries)
      .then(loadData)
      .then((data: any) => writeToDir(dataPath, data))
      .then(() => res.json(true))
      .catch(next);
  });

  return router;
};
