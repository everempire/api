import Knex from 'knex';

import { auditTable, edgeTable, stringTable } from '../config';

export interface Data {
  db?: any;
  literal?: string;
  query?: any;
  column?: string;
  // union?: boolean;
}

export type Selection = typeof Selection;

const Selection = (data: Data) => {
  const graphQuery = (from: string, to: string) => () => {
    const { db, literal, query, column } = data;

    let newQuery;
    if(literal)
      newQuery = (db: Knex) => db.from(edgeTable).where(from, literal);
    else
      newQuery = (db: Knex) => db.from(edgeTable).whereIn(from, db => query(db).select(column));

    return Selection({ db, query: newQuery, column: to });
  };

  const intersect = (selection: any) => {
    const { query: queryA, column: columnA } = data;
    const { query: queryB, column: columnB } = selection.data();

    const query = (db: Knex) => queryA(db).whereIn(columnA, queryB(db).select(columnB));
    return Selection({ ...data, query });
  };

  const union = (selection: any) => {
    const { query: queryA, column: columnA } = data;
    const { query: queryB, column: columnB } = selection.data();

    const a = (db: Knex) => queryA(db).select({ id: columnA });
    const b = (db: Knex) => queryB(db).select({ id: columnB });

    const query = (db: Knex) => db(db.union(a(db), b(db)));
    return Selection({ ...data, query, column: 'id' });
  };

  const result = {
    data: () => data,

    db: (db: Knex) => Selection({ ...data, db }),

    head: graphQuery('tail', 'head'),
    tail: graphQuery('head', 'tail'),

    edgeHead: graphQuery('id', 'head'),
    edgeTail: graphQuery('id', 'tail'),

    // names: () => {
    //   const { query } = data;

    //   const newQuery = (db: Knex) => db.from(edgeTable).
    //   return Selection({ ...data });
    // },

    asEdge: () => Selection({ ...data, column: 'id' }),
    asString: () => {
      const { query, column } = data;

      const newQuery = (db: Knex) => db.from(stringTable).whereIn('id', query(db).select(column));
      return Selection({ ...data, query: newQuery, column: 'string' });
    },

    intersect,
    union,

    and: intersect,
    or: union,

    then: (fn: any) => {
      const { db, literal, query, column } = data;

      if(!db)
        throw new Error('db was never set');

      if(literal)
        return Promise.resolve(literal).then(fn);

      return query(db).distinct().pluck(column)
        .then(fn)
        .catch((error: any) => console.log(error));
    },

    auditRows: (name: string) => {
      const { db, query, column } = data;

      if(!db)
        throw new Error('db was never set');

      return query(db).distinct().select(db.raw(':column: as id, :name as label', { column, name }));
    },
  };

  return result;
};

const tableQuery = (table: string, columnName: string) => (value: any) => {
  const fromQuery = (db: Knex) => db.from(table);

  let query = fromQuery;
  if(value)
    query = db => fromQuery(db).where(columnName, value);

  return Selection({ query, column: 'id' });
};

const intersect = (a: any, b: any) => a.intersect(b);
const union = (a: any, b: any) => a.union(b);

export type Select = typeof Select;

export const Select = {
  node: (node: string) => Selection({ literal: node }),
  audit: tableQuery(auditTable, 'label'),
  string: tableQuery(stringTable, 'string'),

  intersect,
  union,

  and: intersect,
  or: union,
};

export const bindSelect = (db: Knex) => {
  const result: any = {};

  Object.entries(Select).forEach(([key, value]) => {
    result[key] = (...args: any[]) => (value as any)(...args).db(db);
  });

  return result as typeof Select;
};
