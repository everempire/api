import { expect } from 'chai';
import { edgeTable, stringTable, sqlite3Config } from '../config';
import { Edge, } from '../types';
import { initBuilder } from '../utils';

const run = (test: any, testDB: any) => async () => {
  const build = await initBuilder({ knexConfig: sqlite3Config });
  const [Create, db] = await build(['Create', 'db']);

  await test({ Create });

  const edges = (await db(edgeTable))
    .sort((a: any, b: any) => {
      if(a.head === b.head)
        return a.tail > b.tail ? 1 : -1
      else
        return a.head > b.head ? 1 : -1
    });
  const strings = (await db(stringTable))
    .reduce((agg: any, string: any) => {
      agg[string.id] = string.string;
      return agg;
    }, {});

  const flat = {
    edges: edges.map((edge: Edge) => [edge.head, edge.tail]),
    strings: Object.values(strings).sort(),
  };

  // console.log(edges);
  // console.log(flat.edges);

  // console.log(strings);
  // console.log(flat.strings);

  await testDB(flat);

  db.destroy();
};

describe('Create', () => {
  it('should work', run(async ({ Create }: any) => {

    await Create('Just a string');

    await Create('Hello', 'World');
    await Create(['One'], 'Two');
    await Create('Three', [['Four']]);
    await Create('Five', [['Six'], 'Seven']);

  }, ({ edges, strings }: any) => {

    expect(edges).to.deep.equal([
      [ 'A73dntMpfQXPi33+YIeBpMYwKapw9lfpqSjZa6Yy9gI=', 'Seven' ],
      [ 'Five', 'Zomcyxvp1zL5y3VRBgG2y9IsNVvalHXcocIjcrSDmE0=' ],
      [ 'Hello', 'World' ],
      [ 'I0ngU+edl4+AgkKdsIjA1nru5tuc2vvgsjtxn926LpI=', 'Two' ],
      [ 'Three', 'w2vuVG2jIDu0pHwCfWUxlnv1sTvNUZ9HNpU/tb3ZN04=' ],
      [ 'w2vuVG2jIDu0pHwCfWUxlnv1sTvNUZ9HNpU/tb3ZN04=', 'Four' ]
    ]);
    expect(strings).to.deep.equal(['Just a string', 'One', 'Six']);
  }));

  it('escapes', run(async ({ Create }: any) => {

    await Create('E', 'EDGE_A', 'EDGE_B');
    await Create('S', 'SET_ONE', 'SET_TWO', 'SET_THREE');
    await Create('L', 'LIST_A', 'LIST_B', 'LIST_C');

  }, ({ edges, strings }: any) => {

    expect(edges).to.deep.equal([
      [ 'EDGE_A', 'EDGE_B' ],
      [ 'FKJ/yyCBeDekH9MCp8v/C/yUOocRB5puOjMuoKno2cI=', 'SET_ONE' ],
      [ 'FKJ/yyCBeDekH9MCp8v/C/yUOocRB5puOjMuoKno2cI=', 'SET_THREE' ],
      [ 'FKJ/yyCBeDekH9MCp8v/C/yUOocRB5puOjMuoKno2cI=', 'SET_TWO' ],
      [ 'UriWhkn0wRmQLurA2CQIhet/9vXGy/e24V5nbUfZ3ew=', 'LIST_C' ],
      [ 'pydUE8CCNpAWlitxPHHO3pbL1x+Uh9sFV6ZbwDsEkhk=', 'LIST_B' ],
      [ 'qEUlZ1CbGejVrHxa79k6lxZgmkjRs6zIVY0RLqAgc9M=', 'LIST_A' ]
    ]);
    expect(strings).to.deep.equal([]);
  }));

  it('advanced usage', run(async ({ Create }: any) => {

    await Create(
      'ALPHA',
      ['ONE', { TWO: 'THREE', FOUR: ['L', 'ALL', 'FOR', ['ONE']] }, 'FIVE', 'SIX'],
      { ANOTHER: 'COUPLE', OBJECTS: 'HERE' },
      'OMEGA'
    );

  }, ({ edges, strings }: any) => {

    expect(edges).to.deep.equal([
      ['+2FHw+d/5/14V8BXSI17izcKnfwWeXB8t8gSNdO7DNQ=', '2Q5x3yMVhzHqoUvxTZh1wuEz7whiwkuvh0ARaKw00II='],
      ['AKefLuPJWN/HQwiSz/N7u2fCXgJPmt1xXPVFjgRIOSk=', 'OOydtW59HNyGs7L50R1GwW16qP/j9MySu/AFm1LevIE='],
      ['AKefLuPJWN/HQwiSz/N7u2fCXgJPmt1xXPVFjgRIOSk=', 'XHRyzwyYVIHP4kNL1nxzQCnHRvYNabZH+oDHIn3Zw64='],
      ['ANOTHER', 'COUPLE' ],
      ['FOUR', 'xHAcEj/YyFMKHraptxEbRMQbKBebc9cwJEtBLtash8E='],
      ['IlZ2HSj+Loz/AX9V8FVlyYHcivfAsvZKhlmWLNfKnVQ=', 'FOR'],
      ['LwVEUnTpHyXsyunKY5IWf/XRfAEjSHHaWUwfOeMYZio=', 'FIVE'],
      ['LwVEUnTpHyXsyunKY5IWf/XRfAEjSHHaWUwfOeMYZio=', 'ONE'],
      ['LwVEUnTpHyXsyunKY5IWf/XRfAEjSHHaWUwfOeMYZio=', 'SIX'],
      ['LwVEUnTpHyXsyunKY5IWf/XRfAEjSHHaWUwfOeMYZio=', 'tYl9HjRn+AAh6Pf/d9nqmBI7xJzDRoyKFPZLpYFc1Uo='],
      ['OBJECTS', 'HERE'],
      ['TWO', 'THREE'],
      ['eTt1BPEXv15Q10KDMN+2GxFcYTfSnkDd3cRj5G6/hBI=', 'AKefLuPJWN/HQwiSz/N7u2fCXgJPmt1xXPVFjgRIOSk='],
      ['eTt1BPEXv15Q10KDMN+2GxFcYTfSnkDd3cRj5G6/hBI=', 'ALPHA'],
      ['eTt1BPEXv15Q10KDMN+2GxFcYTfSnkDd3cRj5G6/hBI=', 'LwVEUnTpHyXsyunKY5IWf/XRfAEjSHHaWUwfOeMYZio='],
      ['eTt1BPEXv15Q10KDMN+2GxFcYTfSnkDd3cRj5G6/hBI=', 'OMEGA'],
      ['tYl9HjRn+AAh6Pf/d9nqmBI7xJzDRoyKFPZLpYFc1Uo=', 'FFFjlTGz02B3DYaZoSQyqTESvJHEUCL0otCoTtq4whU='],
      ['tYl9HjRn+AAh6Pf/d9nqmBI7xJzDRoyKFPZLpYFc1Uo=', 'gMqxaZ7lQbKjFGLi0UzKCrI6Zs3PRCzyxaFC9Lk3gCE='],
      ['xHAcEj/YyFMKHraptxEbRMQbKBebc9cwJEtBLtash8E=', 'ALL']
    ]);
    expect(strings).to.deep.equal(['ONE']);
  }));
});
