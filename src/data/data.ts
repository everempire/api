import { promises } from 'fs';
import { promisify } from 'util';
import { gzip as gzipCallback, gunzip as gunzipCallback } from 'zlib';

import { Data } from '../types';

const { readFile, writeFile } = promises;

const gzip = promisify(gzipCallback);
const gunzip = promisify(gunzipCallback);

export const readFromFile = async (dataPath: string) => {
  const data = readFile(dataPath)
      .then(jsonBytes => gunzip(jsonBytes))
      .then(bytes => JSON.parse(bytes.toString()) as Data);

  return data;
};

export const readFromDir = async (dataPath: string) => {
  const filePs = ['edge', 'string'].map(name => {
    return readFile(`${dataPath}/${name}.json.gz`)
      .then(jsonBytes => gunzip(jsonBytes))
      .then(bytes => JSON.parse(bytes.toString()));
  });

  const [edges, strings] = await Promise.all(filePs);

  return { edges, strings } as Data;
};

const cleanData = ({ edges, strings }: Data) => {
  const edgeData = edges.map(({ head, tail }) => ({ head, tail }));
  const stringData = strings.map(({ string }) => string);

  return { edges: edgeData, strings: stringData };
};

export const writeToFile = async (dataPath: string, data: Data) => {
  const newData = cleanData(data);
  const jsonData = JSON.stringify(newData, null, 2) + '\n';

  const bytes = await gzip(Buffer.from(jsonData));
  await writeFile(dataPath, bytes);
};

export const writeToDir = async (dataPath: string, data: Data) => {
  const { edges: edgeData, strings: stringData} = cleanData(data);

  const jsonData = {
    edge: JSON.stringify(edgeData, null, 2) + '\n',
    string: JSON.stringify(stringData, null, 2) + '\n',
  };

  const promises = Object.entries(jsonData).map(([name, data]) => {
    return gzip(Buffer.from(data))
      .then(dataGz => writeFile(`${dataPath}/${name}.json.gz`, dataGz));
  });

  await Promise.all(promises);
};
