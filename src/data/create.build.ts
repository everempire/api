import { Builder } from '@gamedevfox/katana';

import { NodeId } from '../types';

const convert = async (input: any, create: any) => {
  const inputType = typeof input;
  if(inputType !== 'object')
    throw new Error(`Can only convert object and array, not: ${inputType}`)

  const subEscape = async (item: any) => {
    const isArray = Array.isArray(item);

    let newItem = item;
    if(typeof item !== 'string')
      newItem = isArray ? await create(...item) : await create(item);

    return newItem;
  };

  if(Array.isArray(input)) {
    const result: NodeId[] = [];

    for(const item of input) {
      const newItem = await subEscape(item);
      result.push(newItem);
    }

    return result;
  }

  // Object
  const entries = Object.entries(input);

  const result: any = {};
  for(const [key, value] of entries) {
    const newValue = await subEscape(value);
    result[key] = newValue;
  }
  return result;
};

export interface Create {
  (...args: any[]): Promise<NodeId>;

  edge: (head: NodeId, tail: NodeId) => Promise<NodeId>;
  string: (any);
  set: (...args: NodeId[]) => Promise<NodeId>;
  map: any;
  list: (...args: NodeId[]) => Promise<NodeId>;
}

export const Create = async (build: Builder): Promise<Create> => {
  const [
    writeEdge, writeString, writeSet, writeMap, writeList,
  ] = await build([
    'writeEdge', 'writeString', 'writeSet', 'writeMap', 'writeList',
  ]);

  const escapeFns = {
    e: writeEdge,
    s: writeSet,
    l: writeList,
  };

  type EscapeChar = keyof typeof escapeFns;

  const create = async (...args: any[]) => {
    const { length } = args;

    if(length === 0)
      throw new Error('Must call with at least one argument');

    const first = args[0];
    const typeOfFirst = typeof first;

    if(length === 1) {
      if(typeOfFirst === 'string')
        return writeString(first);
      else if(typeOfFirst === 'object') {
        if(Array.isArray(first)) {
          const escaped = await convert(first, create);
          return writeSet(escaped);
        } else {
          const escaped = await convert(first, create);
          return writeMap(escaped);
        }
      } else
        throw new Error(`Invalid type for 1 argument: ${typeOfFirst}`)
    }

    if(length === 2) {
      const escaped = await convert(args, create);
      return writeEdge(escaped);
    }

    if(typeOfFirst === 'string' && first.length === 1) {
      const escapeChar = first.toLowerCase();
      if(escapeChar in escapeFns) {
        const write = escapeFns[escapeChar as EscapeChar];

        const escaped = await convert(args.slice(1), create);
        return write(escaped);
      }
    }

    const escaped = await convert(args, create);
    return writeSet(escaped);
  };

  create.edge = (head: NodeId, tail: NodeId) => writeEdge({ head, tail });
  create.string = writeString;

  create.set = (...args: NodeId[]) => writeSet(args);
  create.map = writeMap;
  create.list = (...args: NodeId[]) => writeList(args);

  return create;
};
