import Knex from 'knex';

import { auditTable, edgeTable, stringTable } from '../config';
import { NodeId, StringMap } from '../types';

export type Select = ReturnType<typeof build>;

export type Selection = ReturnType<typeof Selection>;

export type QueryDefSelect = (string | StringMap)[];

export interface QueryDefBase {
  table: string;
  where: [string, any?];
  target: string;
}

export interface QueryDef extends QueryDefBase{
  select: QueryDefSelect;
}

export interface IndexedQueryDef extends QueryDefBase {
  tableAlias: string;
  select: StringMap;
}

export type DynamicQueryDef = QueryDef | ((tableAlias: string) => QueryDef);

export const addQuery = (queries: IndexedQueryDef[], queryDef: DynamicQueryDef): IndexedQueryDef[] => {
  const index = queries.length + 1;
  const tableAlias = `table${index}`;

  const query: QueryDef = typeof queryDef === 'function' ? queryDef(tableAlias) : queryDef;
  let { select, target } = query;

  let newSelect: StringMap = {};
  if(select.length === 1 && typeof select[0] === 'string') {
    const [single] = select;
    newSelect[single] = `${tableAlias}.${target}`
  } else {
    select.forEach(item => {
      if(typeof item === 'string') {
        newSelect[item] = `${tableAlias}.${item}`;
      } else if(typeof item === 'object') {
        Object.entries(item).forEach(([key, value]) => {
          newSelect[value] = `${tableAlias}.${key}`;
        });
      } else
        throw new Error(`Invalid select type: ${typeof item} ${item}`);
    });
  }

  const indexedQueryDef: IndexedQueryDef = { ...query, tableAlias, select: newSelect };
  return [...queries, indexedQueryDef];
};

const InitialSelection = (db: Knex, nodes: NodeId[]) => {
  const queryFn = (where: string, target: string) => (...select: QueryDefSelect) => {
    const queries = addQuery([], (tableAlias: string) => ({
      table: edgeTable,
      where: [`"${tableAlias}"."${where}" in (${nodes.map(() => '?').join(', ')})`, nodes],
      select,
      target,
    }));
    return Selection(db, queries);
  };

  const heads = queryFn('tail', 'head');
  const tails = queryFn('head', 'tail');
  const edges = queryFn('id', 'id');

  // TODO: Strings
  // TODO: Audit/Labels

  return { heads, tails, edges };
};

const selectFn = (db: Knex, queries: IndexedQueryDef[]) => (useSelect: boolean = true) => {
  const wheres: [string, any?][] = [];
  const selects: StringMap[] = [];

  const tables = queries.map(query => {
    const { table, tableAlias, where, select } = query;

    if(where)
      wheres.push(where);

    if(Object.keys(select).length)
      selects.push(select)

    return `"${table}" as "${tableAlias}"`
  }).join(', ');

  let query = db.from(db.raw(tables));

  wheres.forEach(where => {
    const [sql, bindings] = where;
    const raw = db.raw(sql, bindings);
    query = query.where(raw);
  });

  query = query.distinct();

  if(useSelect && selects.length) {
    selects.forEach(select => {
      query = query.select(select)
    });
  } else {
    const lastQuery = queries[queries.length - 1];
    const { tableAlias, target } = lastQuery;
    query = query.pluck(`${tableAlias}.${target}`);
  }

  return query;
};

const asFn = (db: Knex, queries: IndexedQueryDef[], target: string) => () => {
  const lastQuery = queries[queries.length - 1];
  lastQuery.target = target;

  const newQueries = [...queries.slice(0, queries.length - 1), lastQuery];
  return Selection(db, newQueries);
};

const queryFn = (
  db: Knex, queries: IndexedQueryDef[],
  table: string, where: string, target: string,
) => (...select: QueryDefSelect) => {
  const lastQuery = queries[queries.length - 1];
  const { tableAlias: lastTableAlias, target: lastTarget } = lastQuery;

  const newQueries = addQuery(queries, (tableAlias: string) => ({
    table, select, target,
    where: [`"${lastTableAlias}"."${lastTarget}" = "${tableAlias}"."${where}"`],
  }));

  return Selection(db, newQueries);
};

const Selection = (db: Knex, queries: IndexedQueryDef[]) => ({
  db, queries,

  select: selectFn(db, queries),

  asEdge: asFn(db, queries, 'id'),
  asHead: asFn(db, queries, 'head'),
  asTail: asFn(db, queries, 'tail'),

  heads: queryFn(db, queries, edgeTable, 'tail', 'head'),
  tails: queryFn(db, queries, edgeTable, 'head', 'tail'),
  edges: queryFn(db, queries, edgeTable, 'id', 'id'),

  strings: queryFn(db, queries, stringTable, 'id', 'string'),
});

export const AndSelection = (db: Knex, selections: Selection[]) => {

  const select = () => {
    const firstSelection = selections.shift();
    if(!firstSelection)
      throw new Error(`Must have at least 2 selections, received: ${selections.length}`);

    const { queries } = firstSelection;
    const lastQuery = queries[queries.length - 1];
    const { tableAlias, target } = lastQuery;

    let query = firstSelection.select(false);

    selections.forEach(selection => {
      const { sql, bindings } = selection.select(false).toSQL();
      query.where(db.raw(`"${tableAlias}"."${target}" IN (${sql})`, bindings))
    });

    return query;
  };

  const queryFn = (where: string, target: string) => (...sel: QueryDefSelect) => {
    const { sql, bindings } = select().toSQL();

    const queries = addQuery([], (tableAlias: string) => ({
      table: edgeTable,
      where: [`"${tableAlias}"."${where}" in (${sql})`, bindings],
      select: sel,
      target,
    }));

    return Selection(db, queries);
  };

  const heads = queryFn('tail', 'head');
  const tails = queryFn('head', 'tail');
  const edges = queryFn('id', 'id');

  return { select, heads, tails, edges };
};

export const OrSelection = (db: Knex, selections: Selection[]) => {

  const select = () => {
    const firstSelection = selections.shift();
    if(!firstSelection)
      throw new Error(`Must have at least 2 selections, received: ${selections.length}`);

    let query = firstSelection.select(false);

    selections.forEach(selection => {
      const { sql, bindings } = selection.select(false).toSQL();
      query = query.union(db.raw(sql, bindings))
    });

    return query;
  };

  const queryFn = (where: string, target: string) => (...sel: QueryDefSelect) => {
    const { sql, bindings } = select().toSQL();

    const queries = addQuery([], (tableAlias: string) => ({
      table: edgeTable,
      where: [`"${tableAlias}"."${where}" in (${sql})`, bindings],
      select: sel,
      target,
    }));

    return Selection(db, queries);
  };

  const heads = queryFn('tail', 'head');
  const tails = queryFn('head', 'tail');
  const edges = queryFn('id', 'id');

  return { select, heads, tails, edges };
};

export const build = (db: Knex) => {
  const select = (...nodes: NodeId[]) => InitialSelection(db, nodes);
  select.node = select;

  const queryFn = (table: string, where: string, target: string) => {
    return (args: string | string[], select: (string | QueryDefSelect) = []) => {
      const argList = typeof args === 'string' ? [args] : args;
      const selectList = typeof select === 'string' ? [select] : select;

      const queries = addQuery([], (tableAlias: string) => ({
        table,
        // TODO: Fix this? Maybe make `where` nullable
        where: argList ? [`"${tableAlias}"."${where}" in (${argList.map(() => '?').join(', ')})`, argList] : ['1 = 1'],
        select: selectList,
        target,
      }));

      return Selection(db, queries);
    };
  };

  select.string = queryFn(stringTable, 'string', 'id');
  select.audit = queryFn(auditTable, 'label', 'id');

  select.and = (...selections: Selection[]) => AndSelection(db, selections);
  select.or = (...selections: Selection[]) => OrSelection(db, selections);

  return select;
};
