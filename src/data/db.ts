import Knex, { CreateTableBuilder } from 'knex';

import { edgeTable } from '../config';

export const tables: { [key: string]: (table: CreateTableBuilder) => void } = {
  edge: table => {
    table.string('id', 44).primary();
    table.string('head', 44).index().notNullable();
    table.string('tail', 44).index().notNullable();
  },
  string: table => {
    table.string('id', 44).primary();
    table.text('string').index().unique().notNullable();
  },
  user: table => {
    table.string('id', 44).primary();
    table.text('email').index().unique().notNullable();
    table.text('name').index().notNullable();
  },

  audit: table => {
    table.string('id', 44).index().notNullable();
    table.string('label').index().notNullable();
    table.unique(['id', 'label']);
  },
};

export const isInitialized = (db: Knex) => db.schema.hasTable(Object.keys(tables)[0]);

export const initDB = async (db: Knex, knexConfig: any) => {
  await buildTables(db);

  if(knexConfig.client === 'pg')
    await enableTrigram(db);
};

export const buildTables = async (db: Knex) => {
  // Build tables that do not exist
  const tablesP = Object.keys(tables).map(async tableName => {
    const exists = await db.schema.hasTable(tableName);
    if(exists)
      return;

    const table = tables[tableName];
    await db.schema.createTable(tableName, table);
  });

  return Promise.all(tablesP);
};

export const enableTrigram = (db: Knex) => {
  // Enable Trigram Support
  const query = 'CREATE EXTENSION IF NOT EXISTS pg_trgm;';
  return db.raw(query);
};

// ////////////////////////////////////////////////

export const edges = (db: Knex) => db.from(edgeTable);
