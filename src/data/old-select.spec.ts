import { expect } from 'chai';

import { sqlite3Config } from '../config';
import { initBuilder } from '../utils';

const data = {
  edges: [
    { head: 'ONE', tail: 'ALPHA' },
    { head: 'ONE', tail: 'BETA' },
    { head: 'ONE', tail: 'DELTA' },

    { head: 'TWO', tail: 'BETA' },
    { head: 'TWO', tail: 'DELTA' },
    { head: 'TWO', tail: 'OMEGA' },
  ],
  strings: [],
};

describe('OldSelect', () => {
  let Select: any, db: any;

  beforeEach(async () => {
    const build = await initBuilder({ knexConfig: sqlite3Config, data });
    [Select, db] = await build(['OldSelect', 'db']);
  });

  afterEach(async () => {
    db.destroy();
  });

  it('head', async () => {
    expect((await Select.node('ALPHA').head()).sort())
      .to.deep.equal(['ONE']);

    expect((await Select.node('BETA').head()).sort())
      .to.deep.equal(['ONE', 'TWO']);

    expect((await Select.node('DELTA').head()).sort())
      .to.deep.equal(['ONE', 'TWO']);

    expect((await Select.node('OMEGA').head()).sort())
      .to.deep.equal(['TWO']);

    expect((await Select.node('FRANK').head()).sort())
      .to.deep.equal([]);
  });

  it('tail', async () => {
    expect((await Select.node('ONE').tail()).sort())
      .to.deep.equal(['ALPHA', 'BETA', 'DELTA']);

    expect((await Select.node('TWO').tail()).sort())
      .to.deep.equal(['BETA', 'DELTA', 'OMEGA']);

    expect((await Select.node('FRANK').tail()).sort())
      .to.deep.equal([]);
  });

  it('intersect / and', async () => {
    const interA = await Select.node('ONE').tail().and(Select.node('TWO').tail());
    expect(interA.sort()).to.deep.equal(['BETA', 'DELTA']);

    const interB = await Select.and(
      Select.node('ONE').tail(),
      Select.node('TWO').tail()
    );
    expect(interB.sort()).to.deep.equal(['BETA', 'DELTA']);
  });

  it('union / or', async () => {
    const unionA = await Select.node('ONE').tail().or(Select.node('TWO').tail());
    expect(unionA.sort()).to.deep.equal(['ALPHA', 'BETA', 'DELTA', 'OMEGA']);

    const unionB = await Select.or(
      Select.node('ONE').tail(),
      Select.node('TWO').tail()
    );
    expect(unionB.sort()).to.deep.equal(['ALPHA', 'BETA', 'DELTA', 'OMEGA']);
  });

  it('advanced usage', async () => {
    expect((await Select.node('ONE').tail().head()).sort())
      .to.deep.equal(['ONE', 'TWO']);

    expect((await Select.node('ALPHA').head().tail()).sort())
      .to.deep.equal(['ALPHA', 'BETA', 'DELTA']);

    expect((await Select.node('DELTA').head().tail()).sort())
      .to.deep.equal(['ALPHA', 'BETA', 'DELTA', 'OMEGA']);
  });
});
