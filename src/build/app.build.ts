import bodyParser from 'body-parser';
import cors from 'cors';
import express, { Router } from 'express';
import globby from 'globby';

export const app = async (build: any) => {
  const app = express();
  app.use(cors());
  app.use(bodyParser.json({ strict: false }));

  const routes = await build('routes');
  routes.forEach((route: Router) => app.use('/', route));

  return app;
};

export const routes = async (build: any) => {
  const paths = await globby(`${__dirname}/../**/*.route.js`);

  const result: Router[] = [];
  for(const path of paths) {
    const module = require(path);
    const routeBuilders: any = Object.values(module);

    const routesP: Promise<Router>[] = routeBuilders.map((routeBuilder: any) => routeBuilder(build))
    const routes = await Promise.all(routesP);

    routes.forEach(route => result.push(route));
  }

  return result;
};
