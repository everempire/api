import { Builder } from '@gamedevfox/katana';

export const map = (build: Builder) => {
  return async (input: any) => {
    const { id, inputs } = input;

    const fn = await build(id);
    const promises = inputs.map(fn);

    return await Promise.all(promises);
  };
};
