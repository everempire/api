import 'source-map-support/register';

import { dataPath, postgresConfig } from './config';
import { initBuilder } from './utils';

const port = 8080;

(async () => {
  console.log('Starting everempire-api ...');

  const build = await initBuilder({ knexConfig: postgresConfig, dataDir: dataPath });
  const app = await build('app');

  app.listen(port, () => console.log(`everempire-api started on port ${port}`));
})().catch(e => console.error(e));
