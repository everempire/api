import { expect } from 'chai';

import { build, nowSeconds } from './jwt';

describe('JWT', () => {
  it('should work', async () => {
    const { generate, parse, verify } = build('ssh-im-a-secret');

    const now = nowSeconds();
    const exp = now + 15; // 15 seconds from now

    const token = generate({ sub: 'GameDevFox@gmail.com', exp });

    const data = parse(token);
    expect(data).to.deep.equal({ sub: 'GameDevFox@gmail.com', exp });

    expect(verify({ token, now })).equal(true);
    expect(verify({ token, now: now + 15 })).equal(true);
    expect(verify({ token, now: now + 16 })).equal(false);
  });
});
