import { Builder } from '@gamedevfox/katana';
import { hashEdge, EDGE, NAME, SET, STRING } from '@kitsune-system/common';
import { READ_FN } from '../nodes';
import { NodeId } from '../types';

export const graphBuild = async (build: any) => {
  const [Select] = await build(['Select']);

  const readNode = async (id: NodeId) => {
    let result: any = null

    const [readFnEdge] = await Select.and(
      Select(READ_FN).tails().asEdge(),
      Select(id).edges().asHead().heads().asEdge(),
    ).select();

    if(!readFnEdge)
      return null;

    const [readFnId] = await Select(readFnEdge).tails().select();

    const readFn = await graphBuild(readFnId);
    if(!readFn)
      throw new Error(`Can't build node: ${readFnId}`);

    const [edgeTail] = await Select(id).edges().asTail().select();
    result = await readFn(edgeTail)

    return result;
  };

  const graphBuild = async (id: NodeId) => {
    let result = null;

    try {
      result = await build(id);
    } catch(e) {
      // Try to build from graph
      result = readNode(id);

      if(result)
        build.literal(id, result); // Cache result
    }

    return result;
  };

  return graphBuild;
};

export const listTypes = async (build: Builder) => {
  const [Select, isEdge, isString] = await build(['Select', 'isEdge', 'isString']);

  return (id: string) => {
    // TODO: Try to make this one query
    const isEdgeP = isEdge(id);
    const isStringP = isString(id);

    const setsP = Select.and(
      Select(SET).tails(),
      Select(id).heads(),
    ).select();

    return Promise.all([isEdgeP, isStringP, setsP])
      .then(([isEdge, isString, sets]) => {
        if(isString)
          sets.unshift(STRING);
        if(isEdge)
          sets.unshift(EDGE);

        return sets;
      });
  };
};

export const writeName = async (build: Builder) => {
  const [writeEdge, writeString] = await build(['writeEdge', 'writeString']);

  return async (nodeId: NodeId, name: string) => {
    const stringId = await writeString(name);

    return writeEdge([NAME, nodeId])
    .then((nameEdge: NodeId) => writeEdge([nameEdge, stringId]))
  };
};

export const readNames = async (build: Builder) => {
  const Select = await build('Select');

  return (nodeId: NodeId) => {
    const nameEdge = hashEdge([NAME, nodeId]);
    return Select(nameEdge).tails().strings().select();
  };
};

export const getStringNames = async (build: Builder) => {
  const [listTails, readString] = await build(['listTails', 'readString']);

  return async (id: string) => {
    const nameEdge = hashEdge([NAME, id]);

    const nameNodes = await listTails(nameEdge);

    // TODO: Make a single query for this op
    const namePs = nameNodes.map(readString);
    return Promise.all(namePs);
  };
};
