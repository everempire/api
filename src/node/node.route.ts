import express from 'express';

const path = '/node';

export const builder = async (build: any) => {
  const [
    graphBuild, writeName, listTails,
    listHeads, listTypes, getStringNames, readNames,
  ] = await build([
    'graphBuild', 'writeName', 'listTails',
    'listHeads', 'listTypes', 'getStringNames', 'readNames',
  ]);

  const router = express.Router();

  // REFACTOR TO SEPERATE FILE
  // Node functions
  router.all(path + '/:id', async (req, res, next) => {
    const { id } = req.params;
    const input = req.body;

    let fn = await graphBuild(id);

    if(!fn) {
      res.status(400).send(`No such function for id: ${id}`);
      return;
    }

    // TODO: Lookup the fn interface and respond accordingly

    const result = fn(input);

    if(typeof result === 'object' && 'then' in result) {
      result.then((output: any) => res.json(output))
      .catch(next);
    } else {
      res.json(result);
    }
  });

  // REFACTOR TO SEPERATE FILE

  router.get(path + '/:id/types', (req, res, next) => {
    const { id } = req.params;

    listTypes(id)
      .then((sets: any) => res.json(sets))
      .catch(next);
  });

  router.get(path + '/:id/heads', (req, res, next) => {
    const { id } = req.params;
    listHeads(id).then((heads: any) => res.json(heads)).catch(next);
  });

  router.get(path + '/:id/tails', (req, res, next) => {
    const { id } = req.params;

    listTails(id)
      .then((tails: any) => res.json(tails))
      .catch(next);
  });

  // Names
  router.post(path + '/:id/names', (req, res, next) => {
    const { id } = req.params;
    const name = req.body;

    // TODO: Capture this pattern for other uses
    writeName(id, name)
      .then((id: any) => res.json(id))
      .catch(next);
  });

  router.get(path + '/:id/names', (req, res, next) => {
    const { id } = req.params;

    readNames(id)
      .then((nameNodes: any) => res.json(nameNodes))
      .catch(next);
  });

  router.get(path + '/:id/string-names', (req, res, next) => {
    const { id } = req.params;

    // TODO: Move this to it's own function
    getStringNames(id)
      .then((names: any) => res.json(names))
      .catch(next);
  });

  return router;
};
