import { homedir } from 'os';

export const auditTable = 'audit';
export const edgeTable = 'edge';
export const stringTable = 'string';
export const userTable = 'user';

export const kitsunePath = homedir() + '/.kitsune';
export const dataPath = kitsunePath + '/data';

export const postgresConfig = {
  client: 'pg',
  connection: {
    host: 'localhost',
    user: 'postgres',
    password: 'password',
  },
};

export const sqlite3Config = {
  client: 'sqlite3',
  connection: {
    filename: ':memory:',
  },
  useNullAsDefault: true,
};
